package gol.gui;

import java.io.IOException;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBase;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;
import javafx.scene.text.Text;
import static gol.golLanguageProperty.ELLIPSE_ICON;
import static gol.golLanguageProperty.ELLIPSE_TOOLTIP;
import static gol.golLanguageProperty.MOVE_TO_BACK_ICON;
import static gol.golLanguageProperty.MOVE_TO_BACK_TOOLTIP;
import static gol.golLanguageProperty.MOVE_TO_FRONT_ICON;
import static gol.golLanguageProperty.MOVE_TO_FRONT_TOOLTIP;
import static gol.golLanguageProperty.RECTANGLE_ICON;
import static gol.golLanguageProperty.RECTANGLE_TOOLTIP;
import static gol.golLanguageProperty.REMOVE_ICON;
import static gol.golLanguageProperty.REMOVE_TOOLTIP;
import static gol.golLanguageProperty.SELECTION_TOOL_ICON;
import static gol.golLanguageProperty.SELECTION_TOOL_TOOLTIP;
import static gol.golLanguageProperty.SNAPSHOT_ICON;
import static gol.golLanguageProperty.SNAPSHOT_TOOLTIP;
import static gol.golLanguageProperty.ADDTEXT_ICON;
import static gol.golLanguageProperty.ADDIMAGE_ICON;
import static gol.golLanguageProperty.ADDTEXT_TOOLTIP;
import static gol.golLanguageProperty.ADDIMAGE_TOOLTIP;
import gol.data.golData;
import static gol.data.golData.BLACK_HEX;
import static gol.data.golData.WHITE_HEX;
import gol.data.golState;
import djf.ui.AppYesNoCancelDialogSingleton;
import djf.ui.AppMessageDialogSingleton;
import djf.ui.AppGUI;
import djf.AppTemplate;
import djf.components.AppDataComponent;
import djf.components.AppWorkspaceComponent;
import static djf.settings.AppPropertyType.NEW_TOOLTIP;
import static djf.settings.AppStartupConstants.APP_PROPERTIES_FILE_NAME;
import static djf.settings.AppStartupConstants.APP_PROPERTIES_FILE_NAME_OTHERLANGUAGE;
import static djf.settings.AppStartupConstants.FILE_PROTOCOL;
import static djf.settings.AppStartupConstants.PATH_IMAGES;
import static gol.css.golStyle.*;
import gol.data.DraggableEllipse;
import java.util.Optional;
import javafx.scene.layout.FlowPane;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextArea;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.GridPane;
import properties_manager.PropertiesManager;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ToggleButton ;
import javafx.scene.control.ToggleGroup;
import javafx.scene.text.Text; 
import javafx.scene.text.Font ;
import java.util.Observable ;
import javafx.scene.control.Toggle;
import javafx.beans.value.ObservableValue;
import javafx.scene.text.FontWeight ;
import javafx.scene.text.FontPosture ;
import javafx.scene.Node;
import javafx.beans.value.ChangeListener;




/**
 * This class serves as the workspace component for this application, providing
 * the user interface controls for editing work.
 *
 * @author Richard McKenna
 * @author ?
 * @version 1.0
 */
public class golWorkspace extends AppWorkspaceComponent {
    // HERE'S THE APP
    AppTemplate app;

    // IT KNOWS THE GUI IT IS PLACED INSIDE
    AppGUI gui;

    // HAS ALL THE CONTROLS FOR EDITING
    FlowPane editToolbar;
    
    Text textSetting;
    
    Node shape;
    String family;
    int i;
    
    // FIRST ROW
    HBox row1Box;
    Button selectionToolButton;
    Button removeButton;
    Button rectButton;
    Button ellipseButton;
    
    HBox rowIBox;
    Button addTextButton;
    Button addImageButton;
    
    HBox rowABox;
    Font font;
    String text;
    Double textSize;
    ToggleButton bold;
    ToggleButton italic;
    ComboBox fontFamily;
    ComboBox fontSize;
    ToggleGroup toggleGroup;
    
    // SECOND ROW
    HBox row2Box;
    Button moveToBackButton;
    Button moveToFrontButton;

    // THIRD ROW
    VBox row3Box;
    Label backgroundColorLabel;
    ColorPicker backgroundColorPicker;

    // FORTH ROW
    VBox row4Box;
    Label fillColorLabel;
    ColorPicker fillColorPicker;
    
    // FIFTH ROW
    VBox row5Box;
    Label outlineColorLabel;
    ColorPicker outlineColorPicker;
        
    // SIXTH ROW
    VBox row6Box;
    Label outlineThicknessLabel;
    Slider outlineThicknessSlider;
    
    // SEVENTH ROW
    HBox row7Box;
    Button snapshotButton;
    
    // THIS IS WHERE WE'LL RENDER OUR DRAWING, NOTE THAT WE
    // CALL THIS A CANVAS, BUT IT'S REALLY JUST A Pane
    Pane canvas;
    
    // HERE ARE THE CONTROLLERS
    CanvasController canvasController;
    LogoEditController logoEditController;    

    // HERE ARE OUR DIALOGS
    AppMessageDialogSingleton messageDialog;
    AppYesNoCancelDialogSingleton yesNoCancelDialog;
    
    // FOR DISPLAYING DEBUG STUFF
    Text debugText;
    
    Node pasteItem;

    /**
     * Constructor for initializing the workspace, note that this constructor
     * will fully setup the workspace user interface for use.
     *
     * @param initApp The application this workspace is part of.
     *
     * @throws IOException Thrown should there be an error loading application
     * data for setting up the user interface.
     */
    public golWorkspace(AppTemplate initApp) {
	// KEEP THIS FOR LATER
	app = initApp;
        
        


	// KEEP THE GUI FOR LATER
	gui = app.getGUI();

        // LAYOUT THE APP
        initLayout();
        
        // HOOK UP THE CONTROLLERS
        initControllers();
        
        // AND INIT THE STYLE FOR THE WORKSPACE
        initStyle();    
    }
    
    /**
     * Note that this is for displaying text during development.
     */
    public void setDebugText(String text) {
	debugText.setText(text);
    }
    
    // ACCESSOR METHODS FOR COMPONENTS THAT EVENT HANDLERS
    // MAY NEED TO UPDATE OR ACCESS DATA FROM
    
    public ColorPicker getFillColorPicker() {
	return fillColorPicker;
    }
    
    public ColorPicker getOutlineColorPicker() {
	return outlineColorPicker;
    }
    
    public ColorPicker getBackgroundColorPicker() {
	return backgroundColorPicker;
    }
    
    public Slider getOutlineThicknessSlider() {
	return outlineThicknessSlider;
    }

    public Pane getCanvas() {
	return canvas;
    }
        
    // HELPER SETUP METHOD
    private void initLayout() {
        i = 0;
        gui = app.getGUI();
        gui.changeButton.setOnAction(e -> {
            this.change();
        });
	// THIS WILL GO IN THE LEFT SIDE OF THE WORKSPACE
	editToolbar = new FlowPane();
	
	// ROW 1
	row1Box = new HBox();
	selectionToolButton = gui.initChildButton(row1Box, SELECTION_TOOL_ICON.toString(), SELECTION_TOOL_TOOLTIP.toString(), true);
	removeButton = gui.initChildButton(row1Box, REMOVE_ICON.toString(), REMOVE_TOOLTIP.toString(), true);
	rectButton = gui.initChildButton(row1Box, RECTANGLE_ICON.toString(), RECTANGLE_TOOLTIP.toString(), false);
	ellipseButton = gui.initChildButton(row1Box, ELLIPSE_ICON.toString(), ELLIPSE_TOOLTIP.toString(), false);
        
        rowIBox = new HBox();
        addTextButton = gui.initChildButton(rowIBox, ADDTEXT_ICON.toString(), ADDTEXT_TOOLTIP.toString(), false);
        addImageButton = gui.initChildButton(rowIBox, ADDIMAGE_ICON.toString(), ADDIMAGE_TOOLTIP.toString(), false);
        
        rowABox = new HBox();
        font = new Font(24);
        bold = new ToggleButton("Bold");
        italic = new ToggleButton("Italic");
        toggleGroup = new ToggleGroup();
        bold.setToggleGroup(toggleGroup);
        italic.setToggleGroup(toggleGroup);
        bold.setUserData("Bold");
        italic.setUserData("Italic");
        rowABox.getChildren().addAll(bold,italic);
        if(app.i == 1){
            bold.setText("粗體");
            italic.setText("斜體");
        }
        fontFamily = new ComboBox();
        fontFamily.getItems().addAll("Times New Roman","Verdana","Serif","Arial","Courier New");
        fontSize = new ComboBox();
        fontSize.getItems().addAll("12","14","16","18","20","22","24","26","28","30","32","34","36","38","40","42","44","46","48","50");
        rowABox.getChildren().addAll(fontFamily,fontSize);
        

	// ROW 2
	row2Box = new HBox();
	moveToBackButton = gui.initChildButton(row2Box, MOVE_TO_BACK_ICON.toString(), MOVE_TO_BACK_TOOLTIP.toString(), true);
	moveToFrontButton = gui.initChildButton(row2Box, MOVE_TO_FRONT_ICON.toString(), MOVE_TO_FRONT_TOOLTIP.toString(), true);

	// ROW 3
	row3Box = new VBox();
        
        
      
	backgroundColorLabel = new Label("Background Color");
        if(app.i == 1)
            backgroundColorLabel.setText("背景顏色");
	backgroundColorPicker = new ColorPicker(Color.valueOf(WHITE_HEX));
	row3Box.getChildren().add(backgroundColorLabel);
	row3Box.getChildren().add(backgroundColorPicker);

	// ROW 4
	row4Box = new VBox();
	fillColorLabel = new Label("Fill Color");
        if(app.i == 1)
            fillColorLabel.setText("填滿顏色");
	fillColorPicker = new ColorPicker(Color.valueOf(WHITE_HEX));
	row4Box.getChildren().add(fillColorLabel);
	row4Box.getChildren().add(fillColorPicker);
	
	// ROW 5
	row5Box = new VBox();
	outlineColorLabel = new Label("Outline Color");
        if(app.i == 1)
            outlineColorLabel.setText("線條顏色");
	outlineColorPicker = new ColorPicker(Color.valueOf(BLACK_HEX));
	row5Box.getChildren().add(outlineColorLabel);
	row5Box.getChildren().add(outlineColorPicker);
	
	// ROW 6
	row6Box = new VBox();
	outlineThicknessLabel = new Label("Outline Thickness");
        if(app.i == 1)
            outlineThicknessLabel.setText("線條粗細");
	outlineThicknessSlider = new Slider(0, 10, 1);
	row6Box.getChildren().add(outlineThicknessLabel);
	row6Box.getChildren().add(outlineThicknessSlider);
	
	// ROW 7
	row7Box = new HBox();
	snapshotButton = gui.initChildButton(row7Box, SNAPSHOT_ICON.toString(), SNAPSHOT_TOOLTIP.toString(), false);
	
	// NOW ORGANIZE THE EDIT TOOLBAR
	editToolbar.getChildren().add(row1Box);
        editToolbar.getChildren().add(rowIBox);
        editToolbar.getChildren().add(rowABox);
	editToolbar.getChildren().add(row2Box);
	editToolbar.getChildren().add(row3Box);
	editToolbar.getChildren().add(row4Box);
	editToolbar.getChildren().add(row5Box);
	editToolbar.getChildren().add(row6Box);
	editToolbar.getChildren().add(row7Box);
	
	// WE'LL RENDER OUR STUFF HERE IN THE CANVAS
	canvas = new Pane();
	debugText = new Text();
	canvas.getChildren().add(debugText);
	debugText.setX(100);
	debugText.setY(100);
	
	// AND MAKE SURE THE DATA MANAGER IS IN SYNCH WITH THE PANE
	golData data = (golData)app.getDataComponent();
	data.setShapes(canvas.getChildren());

	// AND NOW SETUP THE WORKSPACE
	workspace = new BorderPane();
	((BorderPane)workspace).setLeft(editToolbar);
	((BorderPane)workspace).setCenter(canvas);
    }
    
    // HELPER SETUP METHOD
    private void initControllers() {
	// MAKE THE EDIT CONTROLLER
	logoEditController = new LogoEditController(app);
	
	// NOW CONNECT THE BUTTONS TO THEIR HANDLERS
        toggleGroup.selectedToggleProperty().addListener((ObservableValue<? extends Toggle> ov, Toggle toggle, Toggle new_toggle) -> {
            if(new_toggle == null){
                textSetting.setFont(Font.font(family,textSize));
            }
            else{
                String s = (String)toggleGroup.getSelectedToggle().getUserData();
                if(s.equals("Italic")){
                    font.font(null,FontPosture.ITALIC,textSize);
                    textSetting.setFont(Font.font(family,FontPosture.ITALIC,textSize));
                    i = 1;
                }
                else if(s.equals("Bold")){
                    font.font(null,FontWeight.BOLD,textSize);
                    textSetting.setFont(Font.font(family,FontWeight.BOLD,textSize));
                    i = 2;
                }
            }
                
    });
        fontFamily.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue ov, String t, String t1){
                family = t1;
                if(i == 1)
                    textSetting.setFont(Font.font(family,FontPosture.ITALIC,textSize));
                else if(i == 2)
                    textSetting.setFont(Font.font(family,FontWeight.BOLD,textSize));
                else
                    textSetting.setFont(Font.font(family,textSize));
            }
        });
        fontSize.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue ov, String t, String t1){
                textSize = Double.valueOf(t1);
                if(i == 1)
                    textSetting.setFont(Font.font(family,FontPosture.ITALIC,textSize));
                else if(i == 2)
                    textSetting.setFont(Font.font(family,FontWeight.BOLD,textSize));
                else
                    textSetting.setFont(Font.font(family,50));
            }
        });
	selectionToolButton.setOnAction(e->{
	    logoEditController.processSelectSelectionTool();
	});
	removeButton.setOnAction(e->{
	    logoEditController.processRemoveSelectedShape();
	});
	rectButton.setOnAction(e->{
	    logoEditController.processSelectRectangleToDraw();
	});
	ellipseButton.setOnAction(e->{
	    logoEditController.processSelectEllipseToDraw();
	});
        addTextButton.setOnAction(e->{
            logoEditController.processAddText();
        });
        addImageButton.setOnAction(e->{
            logoEditController.processAddImage();
        });
	
	moveToBackButton.setOnAction(e->{
	    logoEditController.processMoveSelectedShapeToBack();
	});
	moveToFrontButton.setOnAction(e->{
	    logoEditController.processMoveSelectedShapeToFront();
	});

	backgroundColorPicker.setOnAction(e->{
	    logoEditController.processSelectBackgroundColor();
	});
	fillColorPicker.setOnAction(e->{ 
	    logoEditController.processSelectFillColor();
	});
	outlineColorPicker.setOnAction(e->{
	    logoEditController.processSelectOutlineColor();
	});
	outlineThicknessSlider.valueProperty().addListener(e-> {
	    logoEditController.processSelectOutlineThickness();
	});
	snapshotButton.setOnAction(e->{
	    logoEditController.processSnapshot();
	});
	
	// MAKE THE CANVAS CONTROLLER	
	canvasController = new CanvasController(app);
	canvas.setOnMousePressed(e->{
	    canvasController.processCanvasMousePress((int)e.getX(), (int)e.getY());
	});
	canvas.setOnMouseReleased(e->{
	    canvasController.processCanvasMouseRelease((int)e.getX(), (int)e.getY());
	});
	canvas.setOnMouseDragged(e->{
	    canvasController.processCanvasMouseDragged((int)e.getX(), (int)e.getY());
	});
    }

    // HELPER METHOD
    public void loadSelectedShapeSettings(Shape shape) {
	if (shape != null) {
	    Color fillColor = (Color)shape.getFill();
	    Color strokeColor = (Color)shape.getStroke();
	    double lineThickness = shape.getStrokeWidth();
	    fillColorPicker.setValue(fillColor);
	    outlineColorPicker.setValue(strokeColor);
	    outlineThicknessSlider.setValue(lineThickness);
            this.shape = shape;
            if(shape instanceof Text){
                textSetting = (Text)shape;
                text = textSetting.getText();
                font = textSetting.getFont();
                textSize = font.getSize();
            }
	}
        
    }
    public void paste(){
        golData data = (golData)app.getDataComponent();
        if(pasteItem instanceof Text)
            data.addNode((Text)pasteItem);
        else if(pasteItem instanceof ImageView)
            data.addNode((ImageView)pasteItem);
        else
            data.addShape((Shape)pasteItem);
    }
    public void copy(){
        if(shape != null){
            if(shape instanceof Text){
                Text text = new Text();
                Text a = (Text)shape;                
                text.setText(a.getText());
                text.setFont(font);
                pasteItem = text;               
            }
            else if(shape instanceof ImageView){
                ImageView image = (ImageView)shape;
                pasteItem = image;
            }
            else if(shape instanceof DraggableEllipse){
                Shape newShape = (Shape)shape;
            }              
        }
    }

    /**
     * This function specifies the CSS style classes for all the UI components
     * known at the time the workspace is initially constructed. Note that the
     * tag editor controls are added and removed dynamicaly as the application
     * runs so they will have their style setup separately.
     */
    public void initStyle() {
	// NOTE THAT EACH CLASS SHOULD CORRESPOND TO
	// A STYLE CLASS SPECIFIED IN THIS APPLICATION'S
	// CSS FILE
	canvas.getStyleClass().add(CLASS_RENDER_CANVAS);
	
	// COLOR PICKER STYLE
	fillColorPicker.getStyleClass().add(CLASS_BUTTON);
	outlineColorPicker.getStyleClass().add(CLASS_BUTTON);
	backgroundColorPicker.getStyleClass().add(CLASS_BUTTON);
	
	editToolbar.getStyleClass().add(CLASS_EDIT_TOOLBAR);
	row1Box.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        rowIBox.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
	row2Box.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
	row3Box.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
	backgroundColorLabel.getStyleClass().add(CLASS_COLOR_CHOOSER_CONTROL);
	
	row4Box.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
	fillColorLabel.getStyleClass().add(CLASS_COLOR_CHOOSER_CONTROL);
	row5Box.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
	outlineColorLabel.getStyleClass().add(CLASS_COLOR_CHOOSER_CONTROL);
	row6Box.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
	outlineThicknessLabel.getStyleClass().add(CLASS_COLOR_CHOOSER_CONTROL);
	row7Box.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
    }

    /**
     * This function reloads all the controls for editing logos
     * the workspace.
     */
    @Override
    public void reloadWorkspace(AppDataComponent data) {
	golData dataManager = (golData)data;
	if (dataManager.isInState(golState.STARTING_RECTANGLE)) {
	    selectionToolButton.setDisable(false);
	    removeButton.setDisable(true);
	    rectButton.setDisable(true);
	    ellipseButton.setDisable(false);
	}
	else if (dataManager.isInState(golState.STARTING_ELLIPSE)) {
	    selectionToolButton.setDisable(false);
	    removeButton.setDisable(true);
	    rectButton.setDisable(false);
	    ellipseButton.setDisable(true);
	}
	else if (dataManager.isInState(golState.SELECTING_SHAPE) 
		|| dataManager.isInState(golState.DRAGGING_SHAPE)
		|| dataManager.isInState(golState.DRAGGING_NOTHING)) {
	    boolean shapeIsNotSelected = dataManager.getSelectedShape() == null;
	    selectionToolButton.setDisable(true);
	    removeButton.setDisable(shapeIsNotSelected);
	    rectButton.setDisable(false);
	    ellipseButton.setDisable(false);
	    moveToFrontButton.setDisable(shapeIsNotSelected);
	    moveToBackButton.setDisable(shapeIsNotSelected);
	}
	
	removeButton.setDisable(dataManager.getSelectedShape() == null);
	backgroundColorPicker.setValue(dataManager.getBackgroundColor());
    }
    
    @Override
    public void resetWorkspace() {
        // WE ARE NOT USING THIS, THOUGH YOU MAY IF YOU LIKE
        app.loadProperties(APP_PROPERTIES_FILE_NAME);
        bold.setText("Bold");
        italic.setText("Italic");
        backgroundColorLabel.setText("Background Color");
        fillColorLabel.setText("Fill Color");
        outlineColorLabel.setText("Outline Color");
        outlineThicknessLabel.setText("Outline Thickness");
        gui.reload();
        String tooltip = SELECTION_TOOL_TOOLTIP.toString();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip));
        selectionToolButton.setTooltip(buttonTooltip);
        tooltip = REMOVE_TOOLTIP.toString();
        buttonTooltip = new Tooltip(props.getProperty(tooltip));
        removeButton.setTooltip(buttonTooltip);
        tooltip = RECTANGLE_TOOLTIP.toString();
        buttonTooltip = new Tooltip(props.getProperty(tooltip));
        rectButton.setTooltip(buttonTooltip);
        tooltip = ELLIPSE_TOOLTIP.toString();
        buttonTooltip = new Tooltip(props.getProperty(tooltip));
        ellipseButton.setTooltip(buttonTooltip);
        tooltip = ADDTEXT_TOOLTIP.toString();
        buttonTooltip = new Tooltip(props.getProperty(tooltip));
        addTextButton.setTooltip(buttonTooltip);
        tooltip = ADDIMAGE_TOOLTIP.toString();
        buttonTooltip = new Tooltip(props.getProperty(tooltip));
        addImageButton.setTooltip(buttonTooltip);
        tooltip = MOVE_TO_BACK_TOOLTIP.toString();
        buttonTooltip = new Tooltip(props.getProperty(tooltip));
        moveToBackButton.setTooltip(buttonTooltip);
        tooltip = MOVE_TO_FRONT_TOOLTIP.toString();
        buttonTooltip = new Tooltip(props.getProperty(tooltip));
        moveToFrontButton.setTooltip(buttonTooltip);
    }
    public void resetWorkspace1(){
        app.loadProperties(APP_PROPERTIES_FILE_NAME_OTHERLANGUAGE);
        bold.setText("粗體");
        italic.setText("斜體");
        backgroundColorLabel.setText("背景顏色");
        fillColorLabel.setText("填滿顏色");
        outlineColorLabel.setText("線條顏色");
        outlineThicknessLabel.setText("線條粗細");
        gui.reload();
        String tooltip = SELECTION_TOOL_TOOLTIP.toString();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip));
        selectionToolButton.setTooltip(buttonTooltip);
        tooltip = REMOVE_TOOLTIP.toString();
        buttonTooltip = new Tooltip(props.getProperty(tooltip));
        removeButton.setTooltip(buttonTooltip);
        tooltip = RECTANGLE_TOOLTIP.toString();
        buttonTooltip = new Tooltip(props.getProperty(tooltip));
        rectButton.setTooltip(buttonTooltip);
        tooltip = ELLIPSE_TOOLTIP.toString();
        buttonTooltip = new Tooltip(props.getProperty(tooltip));
        ellipseButton.setTooltip(buttonTooltip);
        tooltip = ADDTEXT_TOOLTIP.toString();
        buttonTooltip = new Tooltip(props.getProperty(tooltip));
        addTextButton.setTooltip(buttonTooltip);
        tooltip = ADDIMAGE_TOOLTIP.toString();
        buttonTooltip = new Tooltip(props.getProperty(tooltip));
        addImageButton.setTooltip(buttonTooltip);
        tooltip = MOVE_TO_BACK_TOOLTIP.toString();
        buttonTooltip = new Tooltip(props.getProperty(tooltip));
        moveToBackButton.setTooltip(buttonTooltip);
        tooltip = MOVE_TO_FRONT_TOOLTIP.toString();
        buttonTooltip = new Tooltip(props.getProperty(tooltip));
        moveToFrontButton.setTooltip(buttonTooltip);
    }
    public void change(){
        Alert myAlert = new Alert(Alert.AlertType.CONFIRMATION);
        myAlert.setTitle("language selection");
        myAlert.setHeaderText(null);
        myAlert.setContentText("Choose a language. ");
        
        ButtonType EnglishButton = new ButtonType("English");
        ButtonType ChineseButton = new ButtonType("Chinese");
        myAlert.getButtonTypes().setAll(EnglishButton, ChineseButton);
        
        Optional<ButtonType> result = myAlert.showAndWait();
        if(result.get() == EnglishButton){
            this.resetWorkspace();
        }
        else if(result.get()== ChineseButton){
            this.resetWorkspace1();
        }
    }
}